def semaine():
    semaine = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]

    cinqPremiers = semaine[0:5]
    weekend = semaine[5:]
    print(f"La première méthode donne:\nCinq premiers jours: {cinqPremiers}\nDeux derniers jours:{weekend}")
    
    cinqPremiers2 = semaine[-7:-2]
    weekend2 = semaine[-2:]
    print(f"La deuxième méthode donne:\nCinq premiers jours: {cinqPremiers2}\nDeux derniers jours:{weekend2}")
    
    dernierJour = semaine[6:]
    print(f"La première méthode donne:\nDernier jour de la semaine est: {dernierJour}")
    
    dernierJour2 = semaine[-1:]
    print(f"La deuxième méthode donne:\nDernier jour de la semaine est: {dernierJour2}")
    
    inverseSemaine = semaine[-1:-8:-1]
    print(f"L'inverse de la semaine est: {inverseSemaine}")

def saisons():
    hiver =["Décembre","Janvier","Février"]
    printemps =["Mars","Avril","Mai"]
    ete =["Juin","Juillet","Aout"]
    automne =["Septembre","Octobre","Novembre"]
    saisons =[hiver,printemps,ete,automne]
    print(f"L'instruction saisons[2] affiche: {saisons[2]}\nL'instruction saisons[1][0] affiche:{saisons[1][0]}\nL'instructions saisons[1][2] affiche:{saisons[1][2]}\nL'instruction saisons[:][1] affiche:{saisons[:][1]}")
    
def table9():
    table = list(range(0,11))
    print("La table de multiplication par 9:")
    for i in table:
        print(f" 9*{i}={9*i}")
        
def nombresPairs():
    print(f"Le nombre des nombres pairs dans l'intervalle [2,10000] est: {len(range(2,10001,2))}")
    
def listeIndice():
    semaine = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
    print(f"Les jours de la semaine sont: {semaine}")
    print(f"La valeur de semaine[4] est: {semaine[4]}")
    temp = semaine[6]
    semaine[6] = semaine[0]
    semaine[0] = temp
    print(f"Les valeurs de la première et dernière cases inversées donnent: {semaine}")
    print(f"La valeur du dernier élément répété 12 fois donne:{semaine[6]*12}")

def listRange():
    lstVide,lstFlottant = list(),[0.0]*5
    print(f"La liste vide lstVide: {lstVide}\nLa liste lstFlottant de cinq flottants nuls: {lstFlottant}")
    lstVide += range(0,1001,200)
    print(f"Les nouveaux éléments de la liste lstVide sont: {lstVide}")
    print(f"Les entiers de 0 à 3 sont: {list((range(4)))}\nLes entiers de 4 à 7 sont: {list(range(4,8))}\nLes entiers de 2 à 8 par pas de 2 sont: {list(range(2,9,2))}")
    lstElmnt = list(range(6)) + lstVide + lstFlottant
    print(f"LEs éléments de la liste lstElmnt sont: {lstElmnt}") 
    