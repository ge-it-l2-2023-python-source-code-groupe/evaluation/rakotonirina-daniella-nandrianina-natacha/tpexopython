def commentaireSemaine():
    semaine = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
    jour = 0
    for jour in range(len(semaine)):
        if jour < 4:
            print(f"\nAu travail,on est {semaine[jour]}!!!Grrr")

        elif jour == 4:
            print(f"\nChouette,c'est {semaine[jour]} magnifique!Hehehe!")

        elif (jour == 5):
            print(f"\nChill pour le weekend!Youpiii!")
            
def sequenceADN():
    adn = ["A","C","G","T","T","A","G","C","T","A","A","C","G"]
    print(f"La séquence d'ADN est: {adn}")
    for i in range(len(adn)):
        
        if adn[i] == "A":
            adn[i] = "T"
        elif adn[i] == "T":
            adn[i] = "A"
        elif adn[i] == "C":
            adn[i] = "G"
        else:
            adn[i] = "C"

        complementaire = [i for i in adn] 
    
    print(f"Sa séquence complémentaire est: {complementaire}") 
    
def minimumListe():
    liste = [8, 4, 6, 1, 5]
    min = liste[0]
    for i in range(len(liste)-1):
        if (liste[i]>liste[i+1]):
            min = liste[i+1]
        else:
            min = liste[i]

    print(f"Le minimum estde la liste {liste} est: {min}")
    
def frequenceAcidesAmines():
    acidesAmines = ["A","R","A","W","W","A","W","A","R","W","W","R","A","G"]
    print(f"La liste des acides aminés: {acidesAmines}")
    countA,countW,countR,countG = 0,0,0,0

    for i in range(len(acidesAmines)):
            if acidesAmines[i]=="A":
                countA += 1
            
            elif acidesAmines[i]=="W":
                countW += 1
            
            elif acidesAmines[i]=="R":
                countR += 1
                
            else:
                countG += 1
    print(f"\nFréquence de chaque acide aminé est : \nAlanine: {countA} \nTryptophane: {countW} \nArgine: {countR} \nGlycine: {countG}")
    
def notesMention():
    notes = [14,9,13,15,12]
    print(f"Les notes sont: {notes}")
    print(f"La note minimale est: {min(notes)} et la note maximale est: {max(notes)}") 
    moyenne=0
    note=0
    somme=0
    while note<len(notes):
        somme += notes[note]
        note += 1
    moyenne=somme/len(notes )
    print(f"La moyenne est : {moyenne:.2f}")
    if(moyenne >= 10 and moyenne < 12):
        print("Mention Passable!")
    elif(moyenne >= 12 and moyenne < 14):
        print("Mention Assez Bien!")
    elif(moyenne >= 14):
        print("Mention Bien!")
             
def exoPairs():
    nombres = list(range(0, 21))
    print(f"Les nombres de 0 à 20 sont: {nombres}")

    listePair = []
    listeImpair = []

    for i in nombres:
        if i % 2 == 0 and i <= 10:
            listePair.append(i)

        elif i % 2 != 0 and i > 10:
            listeImpair.append(i)

    print(f"La liste des nombres pairs <= 10 est: {listePair}")
    print(f"La liste des nombres impairs > 10 est: {listeImpair}")
    
def syracuse():
    print("Syracuse:")
    n = input("Entrez un nombre de lignes (entier positif) : ")
    while not n.isdigit() or int(n) <= 0:
        print("Veuillez entrer un nombre entier positif valide.")
        n = input("Le nombre de lignes : ")

    n = int (n)
    syracuse = [n]
    trivial = []
    while len(syracuse) < 50:
        n = n//2 if n % 2 == 0 else n*3+1
        syracuse += [n]
        if n == 1:
            trivial = syracuse[-3:]
    print(syracuse)
    print(f"Oui,la conjoncture de syracuse est vérifiée!\nLes nombres qui constituent le cycle trivial sont: {trivial}")
    
    
def phiPsi():
    angles_phi_psi = [
    [48.6, 53.4], [-124.9, 156.7], [-66.2, -30.8],
    [-58.8, -43.1], [-73.9, -40.6], [-53.7, -37.5],
    [-80.6, -26.0], [-68.5, 135.0], [-64.9, -23.5],
    [-66.9, -45.5], [-69.6, -41.0], [-62.7, -37.5],
    [-68.2, -38.3], [-61.2, -49.1], [-59.7, -41.1]
    ]
    print("Angles Phi et Psi:")

    phi_ideal, psi_ideal = -57.0, -47.0
    tolerance = 30.0
    for i, (phi, psi) in enumerate(angles_phi_psi, start=0):
        if abs(phi - phi_ideal) <= tolerance and abs(psi - psi_ideal) <= tolerance:
            print(f"{i}  :[{phi}, {psi}] est en hélice")
        else:
            print(f"{i} : [{phi}, {psi}] n'est pas en hélice")
    print("\nLa structure secondaire majoritaire des 15 acides aminés est en hélice \n")
            
def nombresPremiers():
    limit = 100
    print(f"Nombres premiers entre 0 et 100:\nMéthode 1: \nLes nombres premiers < {limit} sont: ")
    nb,count = 2,0
    if nb> 1:
        while nb <= limit:
            if nb == 2:
                count += 1;print(nb,end="  ")
            else:
                i,diviseur = 1,0
                while i<= nb :
                    if nb % i == 0:
                        diviseur += 1
                    i +=1
                if diviseur <= 2:
                    count += 1;print(nb,end="  ")
            nb += 1
    print(f"\nEntre 0 et 100,il y a {count} nombres premiers")
    
    ##############################################################
    print("-"*100)
    limit = 100
    print(f"Méthode 2: \nLes nombres premiers <{limit} sont: ")
    nb,premier,count = 2,[],0
    if nb >1:
        while nb <= limit:
            if nb == 2:
                premier += [nb]; count += 1;print(nb,end="  ") 
            elif nb % 2 != 0:
                temp,i = premier+[nb],0
                while i < len(temp)-1:
                    compose,j,x = False,i+1,temp[i];i +=1
                    while j< len(temp)-1:
                        y = temp[j];j +=1
                        if x*y >nb:
                            continue
                        if nb % y == 0 or (x*y) % nb == 0:
                            compose = True
                            break
                    if compose:
                        break
                if not compose:
                    premier += [nb];count += 1;print(nb,end="  ")
            nb +=1
    print(f"\nEntre 0 et 100,il y a {count} nombres premiers")


def rechercheDichotomique():
    print("Devinette:\nPensez à un nombre entre 1 et 100.")

    min,max= 1, 100
    questions = 1

    while min<= max:
        dicho= (min+ max) // 2
        reponse = input(f"Est-ce que votre nombre est plus grand (+), plus petit (-), ou égal (=) à {dicho}? [+/ -/=] ")

        if reponse == "+":
            min = dicho + 1
        elif reponse == "-":
            max = dicho - 1
        elif reponse == "=":
            print(f"J'ai trouvé en {questions} questions ! Bravo !")
            break
        else:
            print("Veuillez entrer un caractère valide (+, -, =).")

        questions += 1
        




