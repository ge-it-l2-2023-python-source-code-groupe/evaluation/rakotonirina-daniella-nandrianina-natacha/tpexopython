def animaux():
    liste = ["vache","souris","levure","bacterie"]
    print("La première méthode d'affichage des animaux avec la boucle for donne:")
    for i in liste:
        print(i)
    print("La deuxième méthode d'affichage avec la boucle for+range donne:")
    for i in range(4):
        print(liste[i])
    print("La troisième méthode d'affichage avce la boucle while donne:")
    i=0
    while i<4:
        print(liste[i])
        i += 1
        
def boucleSemaine():
    semaine = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
    print("Affichage des jours de la semaine avec la boucle for: ")
    for jour in semaine:
        print(jour)
    print("Affichage de weekend avec la boucle while: ")
    jour = 5
    while jour<len(semaine):
        print(semaine[jour])
        jour += 1
        
def dixNombres():
    print("Affiche des nombres de 1 à 10 sue une seule ligne:")
    for i in range(11):
        print(i,end= " ")

def impairToPair():
    impairs =[1,3,5,7,9,11,13,15,17,19,21]
    print(f"La liste des nombres impairs est: {impairs}")
    pairs = [nb+1 for nb in impairs]
    print(f"La liste des nombres pairs déduite de la liste des impairs est: {pairs}")
    

        
def moyenne():
    notes = [14,9,6,8,12]
    print(f"Les notes sont: {notes}")
    moyenne=0
    note=0
    somme=0
    while note<len(notes):
        somme += notes[note]
        note += 1
    moyenne=somme/5
    print(f"La moyenne = {moyenne:.2f}")
    
def produitConsecutif():
    entiers = list(range(2,21,2))
    print(f"La liste des entiers est: {entiers}")
    i=0
    print("Le produit des nombres consécutifs deux à deux:")
    while i < len(entiers)-1:
        print(f"{entiers[i]*entiers[i+1]}")
        i = i+1

def triangleGauche():
    n = 10
    print("Triangle gauche: ")
    for i in range(1,n+1):
        print(f"{''*(n-i)}{'*'*i}")
        
def triangleInverse():
    n = 10
    print("Triangle inversé: ")
    for i in range(n,0,-1):
        print(f"{'*'*i}")
        
def triangleDroite():
    n = 10
    print("Triangle droite: ")
    for i in range(1,n+1):
        print(f"{'*'*i}")
        
def pyramide():
    star = 1
    print("Pyramide: ")
    ligne = input("Entrez le nombre de lignes pour la pyramide : ")
    while not ligne.isdigit() or int(ligne) <= 0:
        print("Veuillez entrer un nombre entier positif.")
        ligne= input("Le nombre de lignes pour la pyramide : ")
    ligne = int(ligne)
    for i in range(0, ligne):
        print(" " * (ligne - i), "*" * star)
        star += 2

        
def parcoursMatrice():
    dim= input("Matrice:\nEntrez la dimension de la matrice carrée(entier positif): ")
    while not dim.isdigit() or int(dim) <= 0:
        print("Veuillez entrer un nombre entier positif.")
        dim= input("La dimension de la matrice carrée: ")
    dim = int(dim)
    for i in range(1, dim + 1):
        for j in range(1, dim + 1):
            print(f"{i:4} {j:4}")
            

def demiMatrice():
    while True:
        N = input("Demi-matrice:\nEntrez le nombre de lignes (entier positif) : ")
        if N.isdigit() and int(N) > 0:
            break
        else:
            print("Veuillez entrer un nombre entier positif valide.")

    N = int(N)
    tab = [[0 for _ in range(N)] for _ in range(N)]
    print(f"tab: {tab}")

    print("\nligne colonne")
    counter = 0
    for i in range(len(tab)):
        for j in range(len(tab[i])):
            while j > i and j != i:
                print(f"{i + 1:>4d}{j + 1:>5d}")
                counter += 1
                break

    print(f"Pour la matrice {N}x{N}, on a parcouru {counter} cases")
    print("La formule générale est : C_N = (N-1) + C_N-1")


def sautDePuce():
    import random
    puce,count = 0, 0
    while not puce == 5:
        puce += random.choice([-1,1]);print(f"{puce}",end="_")
        count += 1
    print(f"\nNombre de saut de 0 à 5: {count}")

def fibonacci():
    fibo = [0,1]
    for i in range(2, 15):
        fibo.append(fibo[i - 1] + fibo[i - 2])
    print("Les 15 premiers termes de la suite de Fibonacci sont:", fibo)
    print(f"\nLe rapport entre les éléments n et n-1 est: ")
    for i in range(2, 15):
        rapport= fibo[i] / fibo[i - 1]
        print(f"\n{fibo[i]} / {fibo[i - 1]} = {rapport:.8f}")
    print(f"\nLe rapport tend vers une constante qui est le nombre d'or Phi: {fibo[i-1] / fibo[i-2]}")



        
            
