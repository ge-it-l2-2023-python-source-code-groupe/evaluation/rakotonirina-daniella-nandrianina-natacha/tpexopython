def test():
    print("Quand on écrit l'instruction 1+1 dans un interpréteur,on l'éxécute en touchant sur la touche 'Entrée',cela affiche :\n>>> 1 + 1\n2")
    print("""
    Mais dans un script test.py,pour pouvoir afficher le résultat de ce petit calcul,il faut faire appel à la fonction print()\n
    Petite démonstration de l'éxécution:
          
    print(1+1)
          
    Le programme va afficher: 2
    
    D'où le petit 2 qui va s'afficher à la fin de cet exercice car j'ai vraiment testé,pas de panique!
        """)
    print(1+1)

def polyA():
    polyA="A"*20
    print(f"\nl'instruction 'A'*20 donne: {polyA}")
    
def polyA_GC():
    polyA="A"*20
    polyGC="GC"*20
    bases=polyA + polyGC
    print(f"L'instruction 'A'*20+'GC'*20 donne: {bases}")
    
def ecritureFormatee1():
    a="salut"
    b=120
    c=10.318
    print(f"\nLa première écriture formatée donne: {a} {b} {c:2f}")
    
def ecritureFormatee2():
    perc_GC=((4500+2575)/14800)*100
    print(f"\nLe pourcentage de GC est {int(perc_GC)}% \n Le pourcentage de GC est {perc_GC:.1f}% \n Le pourcentage de GC est {perc_GC:.2f}% \n Le pourcentage de GC est {perc_GC:.3f}%")