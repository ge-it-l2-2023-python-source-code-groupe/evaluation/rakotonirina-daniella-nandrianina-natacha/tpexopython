def friedman():
    print(f"Test d'expression mathématique: \nL'expression 7+3\u2076 = {7+3**6}-->est un nombre de Friedman")
    print(f"L'expression (3+4)\u2073 = {(3+4)**3}-->est un nombre de Friedman")
    print(f"L'expression(3\u2076)-5 = {(3**6)-5}-->n'est pas un nombre de Friedman")
    print(f"L'expression (1+2\u2078)*5 = {(1+2**8)*5}-->est un nombre de Friedman")
    print(f"L'expression (2+1\u2078)**7 = {(2+1**8)**7}-->est un nombre de Friedman")

def prediction():
    print(f"Les opérations: \nLa première opération (1+2)\u2073 = {(1+2)**3}")
    print(f"La deuxième opération :'Da' * 4 = {'Da' * 4}")
    print(f"La troisième opération :'Da' + 3 n'enst pas valide")
    print(f"La quatrième opération :('Pa'+'La')*2= {('Pa'+'La')*2}")
    print(f"La cinquième opération :('Da'*4)/2 n'est pas valide")
    print(f"La sixième opération :5/2 = {5/2}")
    print(f"La septième opération :5//2 = {5//2}")
    print(f"La huitième opération :5%2 = {5%2}")
    
def conversion():
    print(f"\nConversions:\nLa première instruction str(4) * int(\"3\") donne: {str(4) * int('3')}")
    print(f"\nLa deuxième instruction int(\"3\") + float(\"3.2\") donne: {int('3') + float('3.2')}")
    print(f"\nLa troisième instruction str(3) * float(\"3.2\") affiche ce message d'erreur: \nTypeError: can't multiply sequence by non-int of type 'float'\n")
    print(f"La quatrième instruction str(3/4) * 2 donne: {str(3/4) * 2}")
