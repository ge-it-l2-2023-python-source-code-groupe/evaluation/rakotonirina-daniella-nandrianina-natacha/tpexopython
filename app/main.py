from tools.console import clear;

def cliquez():
    input("\nAppuyez sur la touche 'ENTREE' pour continuer...")
    clear()


def on_press(entrer):
    if entrer.name == 'enter':
        return False


def Menu():
    print("""
         Bonjour Mr Houlder!
          """)
    print("\nVoici le menu de mon programme:")
    print("""
          1)Chap 2 : Variables
          2)Chap 3 : Affichage
          3)Chap 4 : Listes
          4)chap 5 : Boucle et comparaison
          5)chap 6 : Tests
          6)Quitter
          """)
def chap2():
    print("Chapitre 2 : VARIABLE ")
    print("""
         1) Nombre de Friedman
         2) Prédire le résultat : Opérations
         3) Prédire le résultat : Opérations et conversions de type
         4) Retour au Menu  
          """)   
def chap3():
    print("Chapitre 3: AFFICHAGE ")
    print("""
    1)Test
    2)Poly-A
    3)Poly-A et Poly_GC
    4)Ecriture Formatée 1
    5)Ecriture formatée 2
    6)Retour Menu
          """) 
def chap4():
    print("Chapitre 4: LISTE ")
    print("""4
    1)Semaine
    2)Saisons
    3)Table de multiplication par 9 (list ,range)
    4)Nombre de nombre pair entre [2,10000]
    5)Liste & Indice
    6)List & Range
    7)Retour Menu
          """) 
def chap5():
    print("Chapitre 5: BOUCLE ET COMPARAISON ")
    print("""
    1)Boucle de base
    2)Boucle et jour de la semaine
    3)Nombre de 1 à 10 sur une ligne
    4)Nombre pairs et impairs
    5)Calcul de la moyenne
    6)Produit de nombre consécutifs
    7)Triangle gauche
    8)Triangle inversé 
    9)Triangle droite
    10)Pyramide
    11)Parcours de matrice
    12)Parcours de demi-matrice sans la diagonale
    13) Saut de puce
    14)Suite de Fibonacci
    15)Retour au menu principale
          """) 
def chap6():
     print("Chapitre 6: TEST ")
     print("""
        1)Jour de la semaine
        2)Séquence complémentaire d'un brin d'ADN
        3)Minimum d'une liste
        4)Fréquence d'acide aminés
        5)Notes et mention d'un étudiant
        6)Nombres pairs et impairs
        7)Conjecture de Syracuse
        8)Angles Phi et Psi
        9)Détermination des nombres premiers inférieurs à 100
        10)Recherche d'un nombre par dichotomie
        11)Retour au menu principale
            """) 
        
    
"""_summary_
Endpoint 
"""
if __name__=="__main__":
    from chapitres.variables.exo2_11_1à3 import*
    from chapitres.chapitre3_Affichage.exo3_6_1à5 import *
    from chapitres.chapitre4_Liste.exo4_10_1à6 import *
    from chapitres.chapitre5_Boucles_Comparaisons.exo5_4_1à14 import *
    from chapitres.chapitre6_Tests.exo6_7_1à10 import *
  
    while True:
        Menu()
        choix = input("Saisissez un numéro de chapitre: ")
        print("")
        clear()
        if choix == "1":
            while True:
                chap2()
                choix_1 = input("Choisissez un numéro d'exercice: ")
                print("")
                clear()
                if choix_1 == "1":
                    friedman()
                    cliquez()
                    continue
                elif choix_1=="2":
                   prediction()
                   cliquez()
                   continue
                elif choix_1=="3":
                   conversion()
                   cliquez()
                   continue
                elif choix_1=="4":
                   print("===> Retour au Menu \n")
                   cliquez()
                   break
            
        elif choix=="2":
            while True:
                chap3()
                choix_2=input("Choisissez un numéro d'exercice: ")
                print("")
                clear()
                if choix_2=="1":
                    test()
                    cliquez()
                    continue
                elif choix_2=="2":
                    polyA()
                    cliquez()
                    continue
                elif choix_2=="3":
                    polyA_GC()
                    cliquez()
                    continue
                elif choix_2=="4":
                    ecritureFormatee1()
                    cliquez()
                    continue
                elif choix_2=="5":
                    ecritureFormatee2()
                    cliquez()
                    continue
                elif choix_2=="6":
                    print("===> Retour au Menu \n ")
                    cliquez()
                    break
                
        elif choix=="3":
            while True:
                chap4()
                choix_3=input("Choisissez un numéro d'exercice: ")
                print("")
                clear()
                if choix_3 =="1":
                    semaine()
                    cliquez()
                    continue
                elif choix_3=="2":
                    saisons()
                    cliquez()
                    continue
                elif choix_3=="3":
                    table9()
                    cliquez()
                    continue
                elif choix_3=="4":
                    nombresPairs()
                    cliquez()
                    continue
                elif choix_3=="5":
                    listeIndice()
                    cliquez()
                    continue
                elif choix_3=="6":
                    listRange()
                    cliquez()
                    continue
                elif choix_3=="7":
                    print("===> Retour au Menu \n")
                    cliquez()
                    break
                
                    
        elif choix=="4":
            while True:
                chap5()
                choix_4=input("Choisissez un numéro d'exercice: ")
                print("")
                clear()
                if choix_4=="1":
                    animaux()
                    cliquez()
                    continue
                elif choix_4=="2":
                    boucleSemaine()
                    cliquez()
                    continue
                elif choix_4=="3":
                    dixNombres()
                    cliquez()
                    continue
                elif choix_4=="4":
                    impairToPair()
                    cliquez()
                    continue
                elif choix_4=="5":
                    moyenne()
                    cliquez()
                    continue
                elif choix_4=="6":
                    produitConsecutif()
                    cliquez()
                    continue
                elif choix_4=="7":
                    triangleGauche()
                    cliquez()
                    continue
                elif choix_4=="8":
                    triangleInverse()
                    cliquez()
                    continue
                elif choix_4=="9":
                    triangleDroite()
                    cliquez()
                    continue
                elif choix_4=="10":
                    pyramide()
                    cliquez()
                    continue
                elif choix_4=="11":
                    parcoursMatrice()
                    cliquez()
                    continue
                elif choix_4=="12":
                    demiMatrice()
                    cliquez()
                    continue
                elif choix_4=="13":
                    sautDePuce()
                    cliquez()
                    continue
                elif choix_4=="14":
                    fibonacci()
                    cliquez()
                    continue
                elif choix_4=="15":
                    print("===> Retour au Menu \n ")
                    cliquez()
                    break
                
        elif choix=="5":
            while True:
                chap6()
                choix_5=input("Choisissez un numéro d'exercice: ")
                print("")
                clear()
                if choix_5=="1":
                    commentaireSemaine()
                    cliquez()
                    continue
                elif choix_5=="2":
                    sequenceADN()
                    cliquez()
                    continue
                elif choix_5=="3":
                    minimumListe()
                    cliquez()
                    continue
                elif choix_5=="4":
                    frequenceAcidesAmines()
                    cliquez()
                    continue
                elif choix_5=="5":
                    notesMention()
                    cliquez()
                    continue
                elif choix_5=="6":
                    exoPairs()
                    cliquez()
                    continue
                elif choix_5=="7":
                    syracuse()
                    cliquez()
                    continue
                elif choix_5=="8":
                    phiPsi()
                    cliquez()
                    continue
                elif choix_5=="9":
                    nombresPremiers()
                    cliquez()
                    continue
                elif choix_5=="10":
                    rechercheDichotomique()
                    cliquez()
                    continue
                elif choix_5=="11":
                    print("===> Retour au Menu \n ")
                    clear()
                    cliquez()
                    break
                    
                
     
       
        elif choix=="6":
            print("""
            Merci de votre visite,hihi!!!!!!!!
                  """)
            cliquez()
            break
        
        
       
        
            
            
    
  
    
  
